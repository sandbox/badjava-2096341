<?php

require_once DRUPAL_ROOT . '/includes/password.inc';

/**
 * Implements hook_menu().
 */
function change_user_password_menu() {
    $items = array();

    $items['change-password'] = array(
      'title' => t('Change password'),
      'description' => t('Standalone change password form'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('change_user_password_change_password_form'),
      'access callback' => 'user_is_logged_in',
      'type' => MENU_CALLBACK,
    );

    return $items;
}

/**
 * Implements hook_init().
 * @TODO - clean this up and find a better place for this than hook_init()
 */
function change_user_password_init() {
  // Intercept page request to user/uid/edit with pass-reset-token query value and send to change-password instead.
  // Is this the best place to do this? Can't seem to use hook_user_login() because there is no way to 
  // retrieve the pass-reset-token value. 
  if (isset($_GET['pass-reset-token'])) {
    global $user;
    $pass_reset = isset($_SESSION['pass_reset_' . $user->uid]) && isset($_GET['pass-reset-token']) && ($_GET['pass-reset-token'] == $_SESSION['pass_reset_' . $user->uid]);

    if ($pass_reset && arg(0) == 'user' && arg(1) == $user->uid && arg(2) == 'edit') {
      drupal_goto('change-password', array('query' => array('pass-reset-token' => $_GET['pass-reset-token'])));
    }
  }
}

/**
 * Password change form
 */
function change_user_password_change_password_form($form, &$form_state) {

  global $user;

  // double check the token in the query matches the session
  $pass_reset = isset($_SESSION['pass_reset_' . $user->uid]) && isset($_GET['pass-reset-token']) && ($_GET['pass-reset-token'] == $_SESSION['pass_reset_' . $user->uid]);

  if ($pass_reset) {

    $form['pass'] = array(
      '#type' => 'password_confirm',  
      '#size' => 25,
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
    
  } 
  else {
    $form['description'] = array(
      '#markup' => t('You must be logged in and request a temporary password to change it.')
    );
  }

  return $form;
}

/**
 * Validation handler for change_user_password_change_password_form()
 */
function change_user_password_change_password_form_validate($form, &$form_state) {
  if (!$form_state['values']['pass']) {
    form_set_error('pass', t('You must enter a password.'));
  }
}

/**
 * Submit handler for change_user_password_change_password_form()
 */
function change_user_password_change_password_form_submit($form, &$form_state) {

  // edit password and redirect to the user profile page
  global $user;

  $account = user_load($user->uid);
  $account->pass = user_hash_password($form_state['values']['pass']);
  user_save($account);
  drupal_set_message("Your password was successfully changed.");

  $redirect = 'user/' . $user->uid;

  $form_state['redirect'] = $redirect;
}